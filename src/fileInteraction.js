const fs = require('fs');

const updateFile = (file) => {
    const fileToJSON = JSON.stringify(file);
    fs.writeFile('serverDb.json', fileToJSON, 'utf8', (err) => {
        if (err) {
            throw err
        }
    });
}

const readFile = () => {
    fs.readFileSync('serverDb.json', 'utf8', (err) => {
        if (err) {
            throw err
        }
    })
}

module.exports.updateFile = updateFile;
module.exports.readFile = readFile;