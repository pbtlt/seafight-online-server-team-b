const WebSocketServer = require('ws');
//const { updateFile, readFile } = require('./fileInteraction');

const PORT = 3200;
const wss = new WebSocketServer.Server({ port: PORT });

const date = new Date().toLocaleDateString('en-GB', {
    day: 'numeric',
    month: 'short',
    year: 'numeric',
    hour: 'numeric',
    minute: 'numeric',
    second: 'numeric'
});

let createStatementFlag = false;

const serverDB = {
    clientInfo: [],
    queue: [],
    games: [],
}

wss.getUniqueID = () => {
    return Date.now();
};

const generateNewUser = (socket, db) => {
    db.clientInfo.push(socket.userInfo);
    console.log(`${date} Создание пользователя ${socket.userInfo.name} с почтой: ${socket.userInfo.email}`);
    //updateFile(db);
    reply = {
        id: socket.userInfo.id,
        status: 'Registered',
        message: 'Вы успешно зарегистрировались'
    };
    replyToJSON = JSON.stringify(reply);
    socket.send(replyToJSON);
}

const checkConnection = (socket) => {
    socket.on('close', () => {
        console.log(`${date} Пользователь ${socket.userInfo.id} отключился`);
    });
}

const authorization = (socket, data) => {
    const player = {
        status: 'new',
        id: `player_${wss.getUniqueID()}`,
        name: data.name,
        email: data.email,
    }
    let reply;
    let replyToJSON;
    socket.userInfo = player;
    console.log(socket.userInfo);

    const isUserExistance = serverDB.clientInfo.some(client => {
        if (data.email === client.email && data.name === client.name) {
            console.log(`${date} Подключился пользователь: ${client.id}`);
            socket.userInfo.id = client.id;
            reply = {
                id: client.id,
                status: 'Done',
                message: 'Добро пожаловать в игру'
            };
            replyToJSON = JSON.stringify(reply);
            socket.send(replyToJSON);
            return true;
        } else if (data.email === client.email && data.name !== client.name) {
            reply = {
                status: 'Ошибка',
                message: 'Неверная пара имя/email',
            };
            replyToJSON = JSON.stringify(reply);
            socket.send(replyToJSON);
            return true;
        }
    })
    if (!isUserExistance) {
        generateNewUser(socket, serverDB);
    }
}

const createQueue = (id, grid) => {
    serverDB.queue.push({ id, grid });
    serverDB.clientInfo.some(client => {
        if (id === client.id) {
            client.status = 'readyToPlay';
            client.square = grid;
            //updateFile(serverDB);
            return true;
        }
    });
}

const createGame = () => {
    if (serverDB.queue.length == 2) {
        const newQueue = serverDB.queue.splice(0, 2);
        const game = new GameSession(`game_${wss.getUniqueID()}`, newQueue);
        return game;
    } else {
        return false
    }
}

class GameSession {
    constructor(id, arrPlayers) {
        console.log('_________GAME_________');
        console.log('GAME_ID: ', id);
        console.log('ARR_PLAYERS: ', arrPlayers);
        this._gameId = id;
        this._player1 = arrPlayers[0];
        this._player2 = arrPlayers[1];

        wss.clients.forEach((client) => {
            if (client.userInfo.id === this._player1.id) {
                this.ws1 = client;
            }
            if (client.userInfo.id === this._player2.id) {
                this.ws2 = client;
            }
        });
        //console.log('WS_1: ', this.ws1);
        //console.log('WS_2: ', this.ws2);
    }

    start() {
        const message = {
            status: 'game',
            gameId: this._gameId,
        };
        const messageFirstTurn = {
            status: 'game',
            gameId: this._gameId,
            turn: true,
        };

        const randomChoiseFirstPlayer = Math.random();
        if (randomChoiseFirstPlayer > 0.5) {
            this.ws1.send(JSON.stringify(messageFirstTurn));
            this.ws2.send(JSON.stringify(message));
        } else {
            this.ws1.send(JSON.stringify(message));
            this.ws2.send(JSON.stringify(messageFirstTurn));
        }
    }

    userShotProcessing(data) {
        const messageShoot = {
            status: data.condition,
            x: data.coords.x,
            y: data.coords.y,
            turn: true
        };
        const messageShootIsDone = {
            status: data.condition,
            turn: false,
        }
        if (data.id == this.ws1.userInfo.id) {
            this.ws2.send(JSON.stringify(messageShoot));
            this.ws1.send(JSON.stringify(messageShootIsDone));
        } else {
            this.ws1.send(JSON.stringify(messageShoot));
            this.ws2.send(JSON.stringify(messageShootIsDone));
        }
    }
}

let game;

wss.on('connection', (ws) => {
    ws.userInfo = {}
    ws.on('message', (message) => {
        const messageJSON = JSON.parse(message);
        const data = messageJSON.data;
        let status = messageJSON.status;
        switch (status) {
            case 'Login':
                authorization(ws, data);
                checkConnection(ws);
                break;
            case 'readyToPlay':
                createQueue(data.id, data.grid);
                game = createGame();
                if (game) {
                    serverDB.games.push(game);
                    game.start();
                };
                break;
            case 'gameInProcess':
                console.log(message);
                if (game) {
                    game.userShotProcessing(data);
                };
                break;
        }
        return game;
    });
});
console.log(`Сервер запущен. Порт ${PORT}`);
